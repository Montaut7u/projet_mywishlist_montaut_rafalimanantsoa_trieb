<?php

namespace mywishlist\controleur;

use mywishlist\vue\VueParticipant;

class ParticipantControleur{


  /**
  * selectionne toutes les listes de souhait
  */
  private function affTouteListeSouhait(){
    $listes = Liste::select('no', 'titre', 'description') -> get();
    return $listes;
  }

  /**
  * selectionne la liste de souhait courrante
  */
  private function affListeSouhait(){
    $liste = Liste::select('no', 'titre', 'description') -> where('liste_id', '=', $liste)-> get();
    return $listes;
  }

  /**
  *selectionne un item de la liste de souhait courrante
  */
  private function itemParticulier($i){
    $item = Item::select("id", "nom", "descr") -> where ('liste_id', "=", $this->liste, "and", "id", "=", $i) -> get();
    return $item;
  }
