<?php

namespace mywishlist\controleur;

use mywishlist\models\Liste;
use mywishlist\vue\VueListe;
use mywishlist\vue\VueParticipant;

class ListeControleur{

  /**
  * creer une nouvelle liste
  */
  public function nvelleListe(){
    $vl = new VueListe();
    $vl->creerListe();
    echo $vl->render();
  }

  /**
  * Enregistre une nouvelle liste
  */
  public function enrListe(){
    $app = \Slim\Slim::getInstance();
    //Recuperation des donnees
    $id = filter_var(($app->request->post('id')), FILTER_SANITIZE_NUMBER_INT);
    $vartitre = filter_var(($app->request->post('titre')), FILTER_SANITIZE_STRING);
    $descr = filter_var(($app->request->post('descr')), FILTER_SANITIZE_STRING);
    $expr =  $app->request->post('date');

    //Creation de la liste
    $l = new Liste();
    $l->user_id = $id;
    $l->titre = $vartitre;
    $l->description = $descr;
    $l->expiration = $expr;

    $l->save();
    $this->choixListe();
  }

  /**
  * Affiche toutes les listes disponibles
  */
    public function affListe(){
      $vue = new VueListe();
      $vue->render();
    }

  /**
  * Affiche une liste particuliere
  */
    public function affUneListe($id){
      $vue = new VueListe();
    }
}

 ?>
