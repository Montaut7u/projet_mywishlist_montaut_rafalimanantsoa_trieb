<?php

namespace mywishlist\controleur;

use mywishlist\models\Item;
use mywishlist\vue\VueItem;

class ItemController{

  public function getItem($id){
    $item = Item::where('id', '=', $id) -> first();

    $v = new ItemView ($item);
    $v -> render();
  }


  public function nvlleItem(){
    $vi = new VueItem();
    $vi-> ajoutItem();
    echo $vi->render();
  }

  public function enrItem(){
    $app = \Slim\Slim::getInstance();
    //Recuperation des donnees
    $num = filter_var(($app->request->post('num')), FILTER_SANITIZE_NUMBER_INT);
    $nom = filter_var(($app->request->post('nom')), FILTER_SANITIZE_STRING);
    $desc= filter_var(($app->request->post('descr')), FILTER_SANITIZE_STRING);
    $tarif = filter_var(($app->request->post('tarif')), FILTER_SANITIZE_NUMBER_INT);

    $i = new Item();
    $i->liste_id = $num;
    $i->nom = $nom;
    $i->descr = $desc;
    $i->tarif = $tarif;

    $i->save();

  }



}
