<?php

namespace mywishlist\vue;

class VueListe{


/**
* Creer une nouvelle liste
*/
public function creerListe(){
  $app = \Slim\Slim::getInstance();
  return <<<FIN
  <h1>Création d'une nouvelle liste de souhait</h1>
  <form id="FormulaireNouvelleListe" method='POST' action="">
  <p>Votre identifiant : <input type='text' name = 'id'> </p>
  <p>Titre de votre liste : <input type='text' name='titre'> </p>
  <p>Description de votre liste : <input type='text' name='descr'> </p>
  <p>Expire le : <input type='date' name='date'> </p>
  <input type='submit' value='Créer'>
  </form>
FIN;
  }

  /**
  * Renvoie un contenu HTML
  */
  public function render(){
    return $this->creerListe();
  }
}
