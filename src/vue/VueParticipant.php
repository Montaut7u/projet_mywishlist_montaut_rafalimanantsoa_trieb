<?php

namespace mywishlist\vue;

use \mywishlist\models\Liste;

class VueParticipant{
  public $liste;
  public $html;


  public function __construct($obj){
    $this->liste = $obj;
  }

  /**
  *affiche toute les listes de souhaits
  */
  private function affTouteListeSouhait(){
   $listes = Liste::select('no', 'titre', 'description') -> get();

    $res = "<ul>";
    foreach ($this->liste as $liste) {
      $res = $res . "<li>" . $liste . "</li>";
    }
    $res = $res . "</ul>";
  return $res;
  }


  /**
  *affiche tout les items d'une liste de souhait donnée
  */
  private function affListeSouhait(){
  $this->liste = Liste::select('no', 'titre', 'description') -> where('liste_id', '=', $liste)-> get();
    $res = "";
    foreach ($this->liste as $item) {
      $res .= "<ul>$item";
      foreach ($item->getAttributes() as $key => $value) {
        $res .= "<li>$key : $value</li>";
      }
      $res.="</ul>";
    }
    $res = "<p>" . $this->liste->item() . "</p>";
    return $res;
  }


  /**
  *affiche un item particulier d'une liste donnée
  */
  private function itemParticulier($i){
    //mettre 2 where, trouver comment mettre and entre les where
    $item = Item::select("id", "nom", "descr") -> where ('liste_id', "=", $this->liste) -> where("id", "=", $i) -> get();
    $res = "<p>" . $item . "</p>";
    return $res;
  }

  /**
  * Renvoie un contenu HTML
  */
  public function render($selecAffichage){
    switch ($selecAffichage){
      case 1 :
        $content = $this -> affTouteListeSouhait();
        break;
      case 2 :
        $content = $this->affListeSouhait();
        break;
      case 3 :
        $content = $this-> itemParticulier($i);
        break;
      }

    return
<<<END
<!doctype html>
<html lang="fr">
  <head>
    <meta charset="utf-8">
    <title>My Wish List</title>
    <link rel="stylesheet" href="style.css">
    <script src="script.js"></script>
  </head>
  <body>
    <nav class ="Fonctionnalité application"</nav>
    $content;
  </body>
</html>
END;
  }
}
