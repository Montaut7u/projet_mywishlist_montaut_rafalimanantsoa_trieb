<?php

namespace mywishlist\vue;

class VueItem{

/**
* Ajout d'un item à une liste
*/

public function ajoutItem(){
  $app = \Slim\Slim::getInstance();
  return <<<FIN
  <h1> Ajout d'un item à une liste de souhait </h1>
  <form id="FormulaireAjoutItem" method='POST' action="">
  <p>Numero de la liste : <input type='text' name = 'num'> </p>
  <p>Nom de l'item : <input type='text' name='nom'> </p>
  <p>Description : <input type='text' name='descr'> </p>
  <p>Tarif : <input type='text' name='tarif'> </p>
  <input type='submit' value='Ajouter'>
  </form>
FIN;
  }

  /**
  * Renvoie un contenu HTML
  */
  public function render(){
    return $this->ajoutItem();
  }

}
