<?php

require_once 'vendor/autoload.php';

use \mywishlist\models\Item;
use \mywishlist\models\Liste;
use \mywishlist\vue\VueParticipant;
use \mywishlist\vue\VueListe;
use \mywishlist\controleur\ItemController;
use \mywishlist\controleur\ListeControleur;
use \Illuminate\Database\Capsule\Manager as DB;

//BDD
$db = new DB();
$config = parse_ini_file("src/conf/conf.ini");
$db->addConnection($config);
$db->setAsGlobal();
$db->bootEloquent();

session_start();

$app = new \Slim\Slim();

/**
* Index
*/
$app->get("/", function() {
  echo "<h1> My Wish List </h1>
  <h3> Bienvenue sur MyWishList </h3>
  <p>Vous pouvez créer votre liste de souhaits et la partager avec vos amis !</p>";
});

/**
* Page accueil
*/
$app->get("/accueil", function() {
  echo "<h1> My Wish List </h1>"
$vue = new VueAccueil();
$vue->render();
});

/**
* Affiche toute les listes
*/
$app->get('/liste', function() {
    echo "page affiche toute les listes";
    $listl = Liste::all() ;
   $vue = new VueParticipant( $listl->toArray() ) ;
   $vue->render( 1 ) ;
});

/**
* Affiche une liste spécifique
*/
$app->get('/liste/:numero', function($idListe) {
    echo "page affiche liste spécifique";
    $listl = new Liste();
    $listl = getListe($numero);
    $vue = new VueParticipant( $listl->toArray()) ;
    $vue->render( 2 ) ;
});

/**
* Affiche un item particulier d'une liste particuliere
*/
$app->get('/liste/:numero/:id', function($idListe) {
    echo "page affiche item d'une liste spécifique";
    $listl = new Liste();
    $listl = getListe($numero);
    $itControl = new ItemController();
    $itControl->getItem($id);
    $vue = new VueParticipant( $listl->toArray(), $itControl) ;
    $vue->render( 3 ) ;
});

/**
* Affiche page permettant de creer une nouvelle liste
*/
$app->get('/nouvelle_liste', function(){
  $cListe = new ListeControleur();
  $cListe->nvelleListe();
});

$app->post('/nouvelle_liste', function(){
  $cListe = new ListeControleur();
  $cListe->enrListe();
});

/**
* Affiche page permettant d'ajouter un item a une liste'
*/
$app->get('/ajout_item', function(){
  $cItem = new ItemController();
  $cItem->nvlleItem();
});

$app->post('/ajout_item', function(){
  $cItem = new ItemController();
  $cItem->enrItem();
});

$app->run();
